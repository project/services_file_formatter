<?php

/**
 * @file
 * Administration page callbacks for the Services File Formatter module.
 */

/**
 * Menu callback: admin settings form.
 *
 * @see services_file_formatter_menu()
 * @see services_file_formatter_admin_settings_form_submit()
 */
function services_file_formatter_admin_settings_form() {
  $form = array();

  $form['services_file_formatter_mime_types'] = array(
    '#type' => 'textarea',
    '#title' => t('File MIME types'),
    '#description' => t("Specify MIME types passed in request's <em>Accept</em> header to be handled by the file formatter. Enter one MIME type per line.  The '*' character is a wildcard."),
    '#default_value' => variable_get('services_file_formatter_mime_types', SERVICES_FILE_FORMATTER_MIME_TYPES),
  );

  $form = system_settings_form($form);

  $form['#submit'][] = 'services_file_formatter_admin_settings_form_submit';

  return $form;
}

/**
 * Form submission handler for services_file_formatter_admin_settings_form().
 *
 * @see services_file_formatter_admin_settings_form()
 */
function services_file_formatter_admin_settings_form_submit($form, &$form_state) {
  drupal_flush_all_caches();
}
